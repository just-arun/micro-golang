package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()
	fmt.Println("Start application...")
	err := http.ListenAndServe(":9000", r)
	fmt.Println("[ERROR]", err)
}
